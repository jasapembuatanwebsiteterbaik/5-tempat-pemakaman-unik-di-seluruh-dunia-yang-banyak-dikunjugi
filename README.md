

Saat berkunjung ke suatu daerah tertentu, anda pasti akan menyempatkan mengunjungi destinasi wisata yang banyak menarik perhatian. Taman bermain atau alun-alun adalah dua hal biasa yang pasti akan anda kunjungi, tapi anda mungkin perlu coba beberapa tempat yang ada di artikel ini. Betul sekali, saat ini tempat pemakaman tidak lagi identik dengan kesan menakutkan. Sudah ada banyak tempat pemakaman yang justru berubah menjadi destinasi wisata bagi pengunjung yang ingin menikmati suasana liburan yang seru. Berikut ini kami berikan … rekomendasi tempat pemakaman unik yang layak anda kunjungi.
1. Pemakaman Bayi di Tana Toraja, Sulawesi Selatan, Indonesia 
Tana Toraja memang sudah terkenal dengan Kawasan wisata yang unik. Yang satu ini dikenal dengan sebutan Passiliran, tempat pemakaman bayi yang berada di wilayah Toraja ini tidak bisa dilewatkan dengan cuma-cuma. Pasilliran adalah tempat pemakaman yang khusus digunakan sebagai tempat pemakaman bayi yang berada di wilayah Kambira. Bayi yang berada disini berada di bawah usia 6 bulan. Anda tidak akan mendapati pemandangan pemakaman biasa yang kental dengan nuansa horror, karena disini anda justru bisa merasakan udara segar dan pohon bambu yang menyejukkan suasana.  

Bayi yang telah meninggal tidak dikubur dalam tanah, mereka dimakamkan di pohon tarra. Cara pemakaman ini sudah terkenal sejak lama, apalagi bagi pengikut kepercayaan Aluk Todolo. Jangan khawatir akan tersesat, karena ada pemandu yang siap menemani anda menyusuri setiap langkah di Passiliran. Tidak hanya bisa menikmati keunikan tempat pemakaman, anda juga sekaligus mendapat informasi baru seputar kekayaan budaya adat di Indonesia. 

2. The Merry Cemetery, Samanta, Rumania
Setelah ada pemakaman bayi di Indonesia, di salah satu negara yang terletak di wilayah Eropa Tengah dan Tenggara, yaitu Rumania. Merry Cemetery adalah pemakaman unik yang berada di kota Sapanta. Tidak ada pemandangan batu nisan bewarna hitam yang terlihat menyedihkan. Merry Cemetery justru menonjolkan estetika dalam komplek pemakamannya. 

Di dominasi oleh warna biru, batu nisan yang ada tidak hanya tertulis nama, tanggal lahir dan tanggal wafat. Dengan corak yang unik di batu nisannya ditambah lukisan yang menggambarkan perjalanan hidup orang yang dikubur. Lokasi ini instagramable sekali dengan warna-warna cerah yang ada. 
BACA JUGA : Pemakaman Mewah San Diego Hills https://salesmanagersandiegohills.com/
3. Panteón Antiguo de Xoxocotlán - Oaxaca, Meksiko
Pernah menonton film Coco? Ternyata pemakaman Panteón Antiguo de Xoxocotlán yang berada di wilayah Oaxaca, Meksiko ini lah yang jadi sumber inspirasi film produksi Disney tersebut. Pemakaman yang ada memang benar-benar serupa dengan apa yang digambarkan dalam film Coco. Bunga-bunga yang menghiasi hampir setiap sudut, lilin yang memberikan kesan tersendiri, sampai makanan favorit dari almarhum.

Waktu yang tepat untuk mengunjungi pemakaman ini pada 31 Oktober. Di tanggal tersebut, Panteón Antiguo de Xoxocotlán akan ramai dengan keluarga yang berziarah. 


4. Cimetière du Père Lachaise - Paris, Prancis
Dengan luas 44 hektar, pemakaman ini dibuka pada 1804. Setidaknya ada 69 ribu makam dengan ornamen istimewa disini. Pemakaman ini jadi tempat populer bagi para pembaca setia Oscar Wilde. Wilde yang wafat pada 30 November 1900 dimakamkan di  Cimetière du Père Lachaise, Paris, Perancis. Tidak hanya Wilde, Jim Morrison, Frederic Chopin, Marcel Proust, jadi orang-orang terkenal yang dimakamkan di sini. 

Hal menariknya adalah, ada banyak tanda bekas ciuman dengan warna lipstick yang beragam. Saking banyaknya yang mengunjungi pemakaman ini, nisan yang ada di  pemakaman Wilde penuh dengan lipstick. 

5. Highgate Cemetery - London, Inggris  
Pemerintah Inggris menjadikan Highgate Cemetery sebagai Kawasan Taman Bersejarah. Tempat pemakaman Highgate Cemetery dikelilingi pohon rindang yang menjulang tinggi. Pemakaman ini sempat dinobatkan sebagai pemakaman terseram karena cerita-cerita horror dan kisah misterius yang menjadi buah bibir orang-orang. Mulai dari kisah adanya drakula, hingga mayat yang hilang secara misterius. Tapi walaupun ada banyak kisah yang menyeramkan, antusias pengunjung tetap mengalir untuk mengunjungi makam ini. Arsitekturnya yang menarik dan terlihat kokoh mungkin jadi daya tarik. 

Highgate Cemetery jadi tempat peristirahatan terakhir bagi beberapa tokoh dunia. Seperti George Michael, Theodore Hope, Douglas Adams, hingga Bapak Komunis Dunia Karl Marx. 
